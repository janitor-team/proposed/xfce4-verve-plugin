# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Iris Ilexiris <iris.ilexiris@gmail.com>, 2017
# Jarbas Araujo <jarbasaraujojr@yahoo.com.br>, 2006
# Robin van der Vliet <info@robinvandervliet.nl>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-11-23 00:32+0100\n"
"PO-Revision-Date: 2017-11-25 01:35+0000\n"
"Last-Translator: Iris Ilexiris <iris.ilexiris@gmail.com>\n"
"Language-Team: Esperanto (http://www.transifex.com/xfce/xfce-panel-plugins/language/eo/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: eo\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/verve.c:98
#, c-format
msgid "Could not execute command (exit status %d)"
msgstr ""

#. Generate error message
#: ../panel-plugin/verve-plugin.c:434
msgid "Could not execute command:"
msgstr "La komando ne lanĉeblas:"

#. Create properties dialog
#: ../panel-plugin/verve-plugin.c:1153
msgid "Verve"
msgstr "Verveo"

#: ../panel-plugin/verve-plugin.c:1176
msgid "General"
msgstr "Ĝenerala"

#. Frame for appearance settings
#: ../panel-plugin/verve-plugin.c:1181
msgid "Appearance"
msgstr "Aspekto"

#. Plugin size label
#: ../panel-plugin/verve-plugin.c:1197
msgid "Width (in chars):"
msgstr "Larĝeco (po signo):"

#. Plugin label label
#: ../panel-plugin/verve-plugin.c:1222
msgid "Label:"
msgstr ""

#. Frame for color settings
#: ../panel-plugin/verve-plugin.c:1239
msgid "Colors"
msgstr "Koloroj"

#. Plugin background color label
#: ../panel-plugin/verve-plugin.c:1255
msgid "Background color:"
msgstr ""

#. Plugin foreground color label
#: ../panel-plugin/verve-plugin.c:1279
msgid "Foreground color:"
msgstr ""

#. Frame for behaviour settings
#: ../panel-plugin/verve-plugin.c:1298
msgid "History"
msgstr "Historio"

#. History length label
#: ../panel-plugin/verve-plugin.c:1314
msgid "Number of saved history items:"
msgstr "Nombro da konservitaj historieroj"

#. Second tab
#: ../panel-plugin/verve-plugin.c:1334 ../panel-plugin/verve-plugin.c:1338
msgid "Behaviour"
msgstr "Agimaniero"

#. Pattern types frame label
#: ../panel-plugin/verve-plugin.c:1347
msgid "Enable support for:"
msgstr ""

#. Command type: URL
#: ../panel-plugin/verve-plugin.c:1353
msgid "URLs (http/https/ftp/ftps)"
msgstr ""

#. Command type: email address
#: ../panel-plugin/verve-plugin.c:1361
msgid "Email addresses"
msgstr "Retpoŝtadresoj"

#. Command type: directory path
#: ../panel-plugin/verve-plugin.c:1369
msgid "Directory paths"
msgstr ""

#: ../panel-plugin/verve-plugin.c:1382
msgid "Expand variables with wordexp"
msgstr ""

#. Command type: !bang
#: ../panel-plugin/verve-plugin.c:1392
msgid "DuckDuckGo queries (starting with !)"
msgstr ""

#. Command type: I'm feeling ducky (\)
#: ../panel-plugin/verve-plugin.c:1400
msgid "DuckDuckGo queries (starting with \\)"
msgstr ""

#. Fallback if the above don't match
#: ../panel-plugin/verve-plugin.c:1408
msgid "If the above patterns don't match:"
msgstr ""

#. Smart bookmark radio button
#: ../panel-plugin/verve-plugin.c:1414
msgid "Use smart bookmark URL"
msgstr ""

#. Executable command radio button (smart bookmark off)
#: ../panel-plugin/verve-plugin.c:1437
msgid "Run as executable command"
msgstr ""

#: ../panel-plugin/verve-plugin.c:1449
msgid ""
"Run command with $SHELL -i -c\n"
"(enables alias and variable expansion)"
msgstr ""

#. vim:set expandtab sts=2 ts=2 sw=2:
#: ../panel-plugin/xfce4-verve-plugin.desktop.in.h:1
msgid "Verve Command Line"
msgstr "Vervea komandlinio"

#: ../panel-plugin/xfce4-verve-plugin.desktop.in.h:2
msgid "Command line interface with auto-completion and command history"
msgstr "Komandlinia interfaco kun aŭto-kompletado kaj komandhistorio"

#. Print error message
#: ../scripts/verve-focus.c:60
msgid "Failed to connect to the D-BUS session bus."
msgstr "Miskonektis la D-BUS-an busosesion"

#. Print error message
#: ../scripts/verve-focus.c:83
msgid ""
"There seems to be no Verve D-BUS provider (e.g. the Verve panel plugin) "
"running."
msgstr "Ŝajne ne estas D-BUS-an servo (ekz. la Verve-panela kromprogramo) lanĉita."
